package hw3;

import java.util.Scanner;

public class InputDate {
	public void accept() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Input a date (dd mm yyyy):");
		String dateStr= sc.nextLine();
		MyDate d = new MyDate(dateStr);
		System.out.println(d.getDay()+"/"+ d.getMonth()+"/"+d.getYear());
	} 
	
	public void print() {
		MyDate d = new MyDate();
		System.out.println(d.getDay()+"/"+ d.getMonth()+"/"+d.getYear());
	}

}
