package hw3;
import java.util.Calendar;

public class MyDate {
	private int day;
	private int month;
	private int year;

	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		 if (!dateIsValid(day, this.month, this.year)) {
	            System.out.println("Error");
	        } else {
	            this.day = day;
	        }
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		if (!dateIsValid(day, this.month, this.year)) {
            System.out.println("Error");
        } else {
            this.month = month;
        }

	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		if(year < 0 )
			System.out.println("Error");
		this.year = year;
	}
	
	public MyDate() {
		Calendar current_date = Calendar.getInstance();
		this.day= current_date.get(Calendar.DAY_OF_MONTH);
		this.month=current_date.get(Calendar.MONTH) +1;
		this.year= current_date.get(Calendar.YEAR);
		
	}
	public MyDate(int day, int month, int year) {
		super();
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public MyDate(String dateStr) {
		 String str[] = dateStr.split(" ");
		 if (dateIsValid(Integer.parseInt(str[0]), Integer.parseInt(str[1]),Integer.parseInt(str[2]))) {
			 this.day = Integer.parseInt(str[0]);
			 this.month = Integer.parseInt(str[1]);
			 this.year = Integer.parseInt(str[2]);
		  }else System.out.println("Error");
	}
	
    public boolean dateIsValid(int day, int month, int year) {
        if (day < 0 || year < 0) {
            return false;
        }
        switch (month) {
            case 1: case 3: case 5:
            case 7: case 8: case 10:
            case 12:
                return (day > 31) ? false : true;
            case 4: case 6: case 9:
            case 11:
                return (day > 30) ? false : true;
            case 2:
                if ((year % 4 == 0 && year % 100 != 0) ||
                        year % 400 == 0) {
                    return (day > 29) ? false : true;
                }
                return (day > 28) ? false : true;
            default:
                return false;
        }
    }

}
